Geoff: We should show this clip as part of our demo :)
    [Parking Lot (youtube)](https://www.youtube.com/watch?v=pCoYE0RoNvs)

```
Haval: 10/17/2015:
   Thanks Geoff :) Since Lauren and Sami are working on Arrival and Depart functions. What do you want Ryan and I to work on? 

Geoff: 10/17/2015: 
   Done, make sure to do a pull before you start working :) 

Haval: 10/17/2015:
   I deleted the Yahoo one, but it still didn't make it private. I have no idea why. Geoff, you are an admin, if you can fix it then go ahead. 

Haval: 10/17/2015:
   Geoff, go ahead with the refactoring!

Geoff: 10/17/2015:
    FYI under the Access management it looks like Sami is listed twice.
    Did you send the invite for him to two emails maybe?
    This is probably why the repo cant be private. 
    If we remove that yahoo one we can probably make the repo private again. 
                  

Geoff: 10/17/2015
  -- implemented timing(), library, and created printStats() stub
  -- Will test & add more to methods as other methods are implemented.

  -- Does anyone object to the following refactoring?
        Change main class to Simulator (requires main file rename)
        Create Simulation super class with printStats(), and stats vars.
        Change Parking_Lot to sub class of Simulation
        Make Simulation class object in Simulator()
     Will that cause any major merge conflicts for anyone?

Haval: 10/14/2015
Hey guys! Added a parking lot class. Still needs a lot of work. This newest update is just a general format if we decide to go with it. It will NOT RUN right now. Need to add constructors and fix class issues. Essentially the Parking Lot class will have public methods like in the main file and we will call those in main, all variables will be class variables of Parking Lot. We would also need a print Method in Parking Lot later. This ensures we have multiple classes and separate everything from just the main. Text or email me with ideas or suggestions. Jeff, I know you are amazing at Java, if I am doing anything wrong as Java is not my main or secondary language, please fix or advise lol. See you guys tomorrow.  


Geoff: 10/11/2015
    Going to start working on assignment a bit today. 
Is anyone working on files offline and/or not pushing 
often to this repo that I should know about before I 
start? Remember to commit and push often. :)

Haval: 10/11/2015
Do not commit any platform or IDE specific code to the Repo.
Make sure to unstage those files before commiting to the master/origin branch. 
Thank you and comment your code!

###########################################################
# File:    README                 Project: Assignment 1  
# Class:   CS 558, Fall 15        Team: Simulators
# 
# Programmer: Ryan Stevens   RedID: 810318529
# Programmer: Haval Ahmed    RedID: 815013661
# Programmer: Sami Badra     RedID: 815012738
# Programmer: Geoff Crews    RedID: 816165149
# Programmer: Lauren Peppard RedID: 813614016
###########################################################
Parking Lot Simulation
###########################################################
 File Manifest
    ....
  
 Compiling
    ....

 Arguments / Prompts
    -- property filename
         parkingSpots
         queueSizeLimit
         carsAtStart
         arrivalRate
         arrivalVariance
         parkTimeLow
         parkTimeHigh
         exitServiceTime
         maxWallTime
    -- All user time configurations are in seconds
    
###########################################################

 Planning
    ....
 
 Division of Work and Modularization
    ....
    
 Restrictions/Constants/Assumptions:
    
    - Arrival rate of cars to exit should just be dependent on arrival time , park time, and search time. Therefore there is no need for user config for arrival rate of cars to exit gate.

    - This simulation is not concerned with entrance gate queue and rate. We can assume the entrance rate is negligible and there is no service time when a car arrives (IE. You just grab a ticket and enter)


    ....  
 Communication
    10/01/15 - BitBucket created and Users added.
     9/16/15 - Group formed.
 
 Additional Notes
    ....
####################[ EOF: README ]########################
```