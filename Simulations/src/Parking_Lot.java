import java.util.LinkedList;
import java.util.Properties;
import java.util.Queue;
import java.util.Random;

/*
 * Update .02 
 * Made a structure to hold each cars info and used it to write the depart method. 
 * 
 * *************************************************************************************************************
 * 
 *  Update 0.1 
 *  Adds Parking Lot Class
 *  As of now. Everything is just Public. Test to see if we want this format
 *  Initialize Method Will Be A Constructor Method
 *  Will Have Two Constructors. Default if no Props File Passed and One Overloaded One if Argument Was Passed
 *  Have to create those!
 *  
*/

public class Parking_Lot extends Simulation {
	//Simulation Configuration
	public int parkingSpots, queueSizeLimit, carsAtStart;
	public int arrivalRate, arrivalVariance;
	public int enterServiceTime, exitServiceTime;
	public int parkTimeLow, parkTimeHigh;
	
	//Parking lot State
	public double spots[];
	public int parkedCars;
	
	public Car cars[];
	public Queue<Double> enterQueue;
	public Queue<Double> exitQueue;
	public boolean enterGateBusy, exitGateBusy;
	public double[] nextEvents;

	
	//Since our simulation has 2 queue we need 4 events. Is that correct?
	public enum Event { Entrance_Arrival, Entrance_Departure, Exit_Arrival, Exit_Departure }
	
	//Index of Event type in nextEvents
	int Entrance_Arrival = 0;
	int Entrance_Departure = 1;
	int Exit_Arrival = 2;
	int Exit_Departure = 3;
	
	// Class Functions/Methods
	
	// Default Constructor
	public void initialize(Properties props) {

		// initialize the sim clock
		simTime = 0.0;
		parkedCars = 0;
		exitGateBusy = false;
		enterGateBusy = false;
		nextEvents = new double[4]; //Set to 4, Event type count
		try {
			parkingSpots = Integer.parseInt(props.getProperty("parkingSpots", "100"));
			queueSizeLimit = Integer.parseInt(props.getProperty("queueSizeLimit", "8"));
			carsAtStart = Integer.parseInt(props.getProperty("carsAtStart", "20"));
			arrivalRate = Integer.parseInt(props.getProperty("arrivalRate", "1200"));
			arrivalVariance = Integer.parseInt(props.getProperty("arrivalVariance", "60"));
			parkTimeLow = Integer.parseInt(props.getProperty("parkTimeLow", "600"));
			parkTimeHigh = Integer.parseInt(props.getProperty("parkTimeHigh", "7200"));
			enterServiceTime = Integer.parseInt(props.getProperty("enterServiceTime", "60"));
			exitServiceTime = Integer.parseInt(props.getProperty("exitServiceTime", "60"));
			maxWallTime = Integer.parseInt(props.getProperty("maxWallTime", "86400"));
			printInterval = Integer.parseInt(props.getProperty("printInterval", "4"));
		} catch (NumberFormatException e) {
			System.out.println("Error: Invalid property number format");
			System.exit(1);
		}
		
		spots = new double[parkingSpots];
		Car [] cars = new Car[carsAtStart];
		enterQueue = new LinkedList<Double>();
		exitQueue = new LinkedList<Double>();
		
		//Print Configuration
		System.out.println("Simulation initialized with the folowing configuration:");
		System.out.print(String.format("%-20s", "  parkingSpots=" + parkingSpots));
		System.out.print(String.format("%-20s", "  queueSizeLimit=" + queueSizeLimit));
		System.out.println(String.format("%-20s", "  carsAtStart=" + carsAtStart));
		System.out.print(String.format("%-20s", "  arrivalRate=" + arrivalRate));
		System.out.print(String.format("%-20s", "  arrivalVariance=" + arrivalVariance));
		System.out.println(String.format("%-20s", "  enterServiceTime=" + enterServiceTime));
		System.out.print(String.format("%-20s", "  exitServiceTime=" + exitServiceTime));
		System.out.print(String.format("%-20s", "  parkTimeLow=" + parkTimeLow));
		System.out.println(String.format("%-20s", "  parkTimeHigh=" + parkTimeHigh));
		System.out.print(String.format("%-20s", "  maxWallTime=" + maxWallTime));
		System.out.print(String.format("%-20s", "  printInterval=" + printInterval));
		System.out.println("");
		
		nextEvents[Entrance_Arrival] = Library.normalDist(arrivalRate, arrivalVariance);
		nextEvents[Entrance_Departure] = 1.0e+30;
		nextEvents[Exit_Arrival] = 1.0e+30;
		nextEvents[Exit_Departure] = 1.0e+30;
	}	
	
	
	
	public void update_time_avg_stats() 
	{
		// TODO Auto-generated method stub
		// Prototype For Function Might Need To Do More
		// float time_Since_Last_Event;
		
		//time_since_last_event = sim_time - time_last_event;
	    //time_last_event = sim_time;

	    /* Update area under number-in-queue function. */

	  //  area_num_in_q      += num_in_q * time_since_last_event;

	    /* Update area under server-busy indicator function. */

	    //area_server_status += server_status * time_since_last_event;
		
		
		// Need To Complete This Haval
	}


	public Event timing() {
		// TODO Auto-generated method stub
		 
		 double min_time_next_event = 1.0e+29;
		 Event next_event_type = null;
		 
		 //find next soonest event
		 for (int i = 0; i < nextEvents.length; i++){
			 if (nextEvents[i] < min_time_next_event) {
				 min_time_next_event = nextEvents[i];
				 next_event_type = Event.values()[i];
			 }
		 }
		 //Uh oh, no more events?
		 if (next_event_type == null)
			 return null;
		 
		 // Advance clock to event
		 simTime = min_time_next_event;

		return next_event_type;
	}

	public void event() {
		 //TODO ???
	}
	

	
	/**
	 * entranceArrive will take the car that arrives put it in the entrance queue
	 * wait the time to send them in. 
	 *  
	 */
	public void entranceArrive() {
		nextEvents[Entrance_Arrival] = simTime + Library.normalDist(arrivalRate, arrivalVariance);
		if(enterGateBusy == true) {
			enterQueue.add(simTime);
		} else {
			enterGateBusy = true;
			nextEvents[Entrance_Departure] = simTime + enterServiceTime;
		}
	}

	public void entranceDepart(int carNumber) {
		 
		//update entrance queue and server
		if (enterQueue.isEmpty()) {
			enterGateBusy = false;
			nextEvents[Entrance_Departure] = 1.0e+30;
		} else {
			nextEvents[Entrance_Departure] = simTime + enterServiceTime;
			enterQueue.poll();
		}
		// park car
		 int randomNumber = Library.random.nextInt(parkingSpots);
		 int count = parkingSpots;
		 while (spots[randomNumber] != 0)
		 {
			 randomNumber = Library.random.nextInt(parkingSpots);
			 if(count-- < 0) { //TODO
				 nextEvents[Exit_Arrival] = simTime; //they gave up and are now leaving
				 //System.exit(1); // To much Time spent searching, handle better
			 }
		 }
		 // Spot value = exit gate arrival time
		 // = simTime + Park time + static time parking and getting to exit gate
		 spots[randomNumber] = simTime + 60 + Library.rangeRand(parkTimeLow, parkTimeHigh);
		 parkedCars++;
		 //set as exitArrive if its the next soonest
		 if(spots[randomNumber] < nextEvents[Exit_Arrival])
			 nextEvents[Exit_Arrival] = spots[randomNumber];
	}
	
	public void exitArrive(){
		//get next Arrival
		parkedCars--;
		double nextExitArrival = 1.0e+30;
		
		// find next soonest car exit event
		for(int i = 0; i < parkingSpots; i++)
			if(spots[i] > 0 && spots[i] < nextExitArrival)
				nextEvents[Exit_Arrival] = nextExitArrival;

		if(exitGateBusy == true) {
			exitQueue.add(simTime);
		} else {
			exitGateBusy = true;
			nextEvents[Exit_Departure] = simTime + exitServiceTime;
		}
	}
	
	public void exitDepart(){
		//update exit queue and server
		if (exitQueue.isEmpty()) {
			exitGateBusy = false;
			nextEvents[Exit_Departure] = 1.0e+30;
		} else {
			nextEvents[Exit_Departure] = simTime + exitServiceTime;
			exitQueue.poll();
		}
	}
	

	public void printStats(boolean finished, Event lastEvent) {
		
		if(finished) {
			System.out.println("Finised...");  //TODO
		} else {
			System.out.println("Simulation Time: " + String.format("%.2f",simTime) + "\tLast Event: " 
						+ String.format("%-24s", lastEvent) + "Parked #: " + parkedCars); //TODO
			
			if(printInterval == 1) // IF 1 assume verbose debugging, slow mode to watch events
				try {Thread.sleep(50);} catch (Exception e) {}
		}
	}
	

	public boolean isStopCase(boolean terminate, Event nextEventType) {
		if(nextEventType == null) {
			System.out.println("Simulation Ended: No more Events.");
			return true;
		}
		if (simTime >= maxWallTime) {
			System.out.println("Simulation Ended: Total Runtime Reached.");
			return true;
		}
		if (enterQueue.size() >= queueSizeLimit || exitQueue.size() >= queueSizeLimit ) {
			System.out.println("Simulation Ended: Maximum Queue Size Reached.");
			return true;
		}
		if (terminate) {
			System.out.println("Simulation Ended: User Stopped.");
			return true;
		}
		return false;
	}
	
}
