import java.util.Random;

public class Library {
	public static final Random random = new Random();
	
	public static double expon(double mean) {
		//TODO, Not Needed?
		return 0;
	}
	public static double normalDist(double mean, double variance) {
		//TODO, check
		return random.nextGaussian() * Math.sqrt(variance) + mean;
	}
	public static double massDensity() {
		//TODO, Apply mass density function
		// Density   : f(x)= 24*3(1-x) = -72x + 72
		// Integral  :  -36x^2 + 72x   = -36(x+1)^2 + 36
		// Inverse   : sqrt(( 36 - x) / 36) + 1
		// .....
		return -Math.sqrt(random.nextDouble())/6 + 1;
	}
	public static double rangeRand(int low, int high) {
		//TODO, check
		return massDensity() * (high-low) + low;
	}
}