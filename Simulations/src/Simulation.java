
public class Simulation {
	
	public int maxWallTime, printInterval;
	public double simTime;
	
	//elements used to analyze data
	public int timeLookingForSpot = 0;
	public int exitWaitTime = 0;
	public int enterWaittime = 0;
	public int totalTimeSystemRuns = 0;
	public int totalCarsInSystem = 0;
	public int timeLotIsFull = 0;
	public int totalOfCarsEnterQueue = 0;
	public int totalOfCarsExitQueue = 0;
	

}
