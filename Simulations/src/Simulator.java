import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.Scanner;

/**
 * Update 0.27 Ryan Stevens
 * I made a structure to hold each cars info. This might be useful when we need to see what spot each car is in and what time
 * the cars arrive. Let me know if we need this or not. 
 * 
 * ****************************************************************************************
 * 
 * Test from Lauren
 * 
 * ****************************************************************************************
 * Update 0.26 Haval Ahmed
 * Prototype Stage
 * Commented Out Code.
 * Plan is to have main pass arg[0] to constructor or default method in parking lot
 * which will be the props string name and get info.
 * From there, we will initialize variables and then call Parking Lots Functions.
 * It is the same idea as before, just split into a new class and file. 
 * Jeff or Somebody who knows Java better than me can clean up if we decide on this
 * Please Provide Feedback!
 * 
 * ****************************************************************************************
 * Update 0.25 Haval Ahmed
 * Going To Add A Public Parking Lot Class
 * Will Just Include Methods in Class. Initialization Method will be default constructor called.
 * Please Check! I could be not properly writing Java. Not my main language. 
 * Main File and Class can keep same format. Just need to add variables and functions to a seperate class/file.
 * 
 * ****************************************************************************************
 * Update 0.241 Sami
 * initial commit
 * 
 * ****************************************************************************************
 * 
 * Update 0.24 Geoff
 *   lots of changes :)
 * **
 * Update 0.21 Ryan
 * 
 * Updated the initialize function to include more parameters and the items needed
 * to calculate the statistical analysis. also made the enter method and the exit
 * method. Code still needs to go in here.
 * From my understanding this is a two single server models. We have one server at the entrance
 * and one at the exit. If the parking lot is full then there needs to be a queue 
 * for cars waiting to get in. If cars leave at the same time then there needs to 
 * be a queue for the end. The ending queue is not as big a deal since 
 * cars will always be able to exit it just takes time.    
 * 
 * **************************************************************************************
 *  
 * Update 0.2 Geoff
 * 	My first git commit (test)   
 * 
 * ***************************************************************************************
 * 
 * Update 0.1 Ryan
 *  
 *  Started the initialize. Prototyping and not complete so if changed that is okay. 
 *  the main thing initialize does allows the user to enter the information
 *  they want. it returns a -1 if an error is thrown and a 1 if no errors
 *  were made by the user. It i missing some parameters we will need 
 *  to collect to analyze the data. 
 * 
 * ************************************************************************************
 * 
 * Update 0.0 Ryan
 *
 * 	This is the main program. 
 *	To determine what this program does we will use figure 1.3*
 *	in our book (Simulation modeling and Analysis 5th Ed. by 
 *	Averill M. Law) as an outline. Since this part is the main 
 *	program will:
 *	0. Invoke the initialization routine
 *	then it will repeatedly
 *	1. Invoke the timing routine.
 *	2. Invoke event routine i.
 *	Figure 1.3 is found on page 10.
 *	
 *	Code here is being referenced from the book also. 
 *	The code that is being used as a reference can be found on page 34.
 *	Since this is preliminary coding and prototyping some, if not most, 
 *	of this code may be changed to fit our specific problem. 
**/

/**  Problem Statement 
 * 
 *  For this program we will be constructing a parking lot simulation.
 *  The system shall contain a parking area that has 2 rows of parking 
 *  spaces, one gated entrance, and one gated exit. The system shall	
 *  have cars that approach the entrance, are admitted, find an empty
 *  parking space, park, leave the parking space, approach the exit,	
 *  and then leave. The system shall have the following user-configurable	
 *  parameters:	
 *  1. Number of parking spots in system
 *  2. Total number of cars in system
 *  3. Arrival rate of cars to entrance and exit gate (in seconds)
 *  4. Range of "park" interval (in seconds)
 *  5. Rate at which exit gate will allow cars to exit (in seconds)	
 *
 */ 

public class Simulator extends Parking_Lot {

	public static boolean terminate;
	
	public static void main(String[] args) {
		
		//stop and try and print when program terminated
		terminate = false;
		Runtime.getRuntime().addShutdownHook(new Thread()
        { @Override public void run() {terminate = true;}});
		
		//load props
		Properties props = loadPropertyFile(args.length > 0 ? args[0] : null);
		
		//parking lot setup
		Parking_Lot parkingLot = new Parking_Lot(); 
		
		parkingLot.initialize(props);
		
		//User input terminate
		Thread input = new Thread()
        { @Override public void run() {
        	Scanner in = new Scanner(System.in);
        	in.nextLine(); in.close();
        	terminate = true;}};
        input.start();
        System.out.println("Simulation Running: Press Enter to stop.");

        Event nextEventType;
        int i = 0;
		while (terminate == false) {

			nextEventType = parkingLot.timing();
			
			//parkingLot.update_time_avg_stats();
			
			if( nextEventType != null) {
				switch(nextEventType) {
					case Entrance_Arrival:
						parkingLot.entranceArrive();
						break;
					case Exit_Arrival:
						parkingLot.exitArrive(); //Need to pass in which gate???
						break;
					case Entrance_Departure:
						parkingLot.entranceDepart(0);
						break;
					case Exit_Departure:
						parkingLot.exitDepart(); //Need to pass in which gate???
						 break;
				}
			}
			
			if(++i % parkingLot.printInterval == 0)
				parkingLot.printStats(false, nextEventType);

			// Stopping cases
			if(parkingLot.isStopCase(terminate, nextEventType))
				break;
		}
		
		parkingLot.printStats(true, null);
	}

	private static Properties loadPropertyFile(String file) {
		Properties props = new Properties();
		
		// look for Simulation properties file in likely folder if no property file arg specified
		// if no properties but find a Simulations folder, print out file selection prompt
		if(file == null){
			File propDir = new File(Parking_Lot.class.getProtectionDomain()
								.getCodeSource().getLocation().getPath() + "../Simulations/");
			File[] filesList;
			if(propDir.exists() && (filesList = propDir.listFiles()).length > 0) {
				System.out.println("Simulation Properties File:\r\n  0: Use Defaults");
				int i = 1;
				for(File f : filesList){
		            if(f.isFile())
		                System.out.println("  " + i++ + ": " + f.getName());
		        }
				System.out.print("Enter Choice: ");
	        	Scanner in = new Scanner(System.in);
	        	int sel = in.nextInt() - 1;
	        	if(sel >= 0 && sel < filesList.length)
	        		file = filesList[sel].getPath();
	        	in.close();
			} else {
				System.out.println("No Property File Spesified, Using Defaults");
			}
		}
		
		if(file != null) {
			try {
				FileInputStream fis;
				fis = new FileInputStream(file);
	        	props.load(fis);
			} catch (Exception e) {
				System.out.println("Error: Could not load propery file.");
				System.out.println("usage: main [property file]");
			}
		}
		return props;
	}
}
