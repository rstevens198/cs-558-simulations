/**
 * Ryan
 * I dont know if we need this but here is an array of cars we can use to store each cars info. 
 * 
 *
 */

public class Car {
	private  int spotNumber;
	private double entranceArriveTime;
	private double entranceDepartTime;
	private double exitArriveTime;
	private double exitDepartTime;
	
	
	
	public void setSpotNumber(int s){
		spotNumber = s;
	}
	
	public int getSpotNumber()
	{
		return spotNumber;
	}
	
	public void setEntranceArriveTime(int t){
		entranceArriveTime = t;
	}
	
	public double getEntranceArriveTime(){
		return entranceArriveTime;
	}
	
	public void setEntranceDepartTime(int t){
		entranceDepartTime = t;
	}
	
	public double getEntranceDepartTime(){
		return entranceDepartTime;
	}
	
	public void setExitArriveTime(int t){
		exitArriveTime = t;
	}

	public double getExitArriveTime(){
		return exitArriveTime;
	}
	
	public void setExitDepartTime(int t){
		exitDepartTime = t;
	}
	
	public double getExitDepartTime(){
		return exitDepartTime;
	}
}
